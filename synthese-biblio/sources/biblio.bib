@inproceedings{seam-carving-video,
    author = {Rubinstein, Michael and Shamir, Ariel and Avidan, Shai},
    title = {Improved Seam Carving for Video Retargeting},
    year = {2008},
    isbn = {9781450301121},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    url = {https://doi.org/10.1145/1399504.1360615},
    doi = {10.1145/1399504.1360615},
    abstract = {Video, like images, should support content aware resizing. We present video retargeting using an improved seam carving operator. Instead of removing 1D seams from 2D images we remove 2D seam manifolds from 3D space-time volumes. To achieve this we replace the dynamic programming method of seam carving with graph cuts that are suitable for 3D volumes. In the new formulation, a seam is given by a minimal cut in the graph and we show how to construct a graph such that the resulting cut is a valid seam. That is, the cut is monotonic and connected. In addition, we present a novel energy criterion that improves the visual quality of the retargeted images and videos. The original seam carving operator is focused on removing seams with the least amount of energy, ignoring energy that is introduced into the images and video by applying the operator. To counter this, the new criterion is looking forward in time - removing seams that introduce the least amount of energy into the retargeted result. We show how to encode the improved criterion into graph cuts (for images and video) as well as dynamic programming (for images). We apply our technique to images and videos and present results of various applications.},
    booktitle = {ACM SIGGRAPH 2008 Papers},
    articleno = {16},
    numpages = {9},
    keywords = {video retargeting, video editing, forward energy, seam carving, image retargeting},
    location = {Los Angeles, California},
    series = {SIGGRAPH '08}
}

@misc{blog-forward-energy,
    author = {Das, Avik},
    title  = {Improved seam carving with forward energy},
    year   = {2019},
    howpublished = {\url{https://avikdas.com/2019/07/29/improved-seam-carving-with-forward-energy.html}},
}

@misc{dual-gradient-energy-function,
    howpublished = {\url{https://courses.cs.washington.edu/courses/cse373/20wi/homework/seamcarving/#dual-gradient-energy-function}},
}


@article{Huang2009RealtimeCI,
  title={Real-time content-aware image resizing},
  author={Hua Huang and TianNan Fu and Paul L. Rosin and Chun Qi},
  journal={Science in China Series F: Information Sciences},
  year={2009},
  volume={52},
  pages={172-182}
}

@article{seam-carving-limites,
author = {Rubinstein, Michael and Gutierrez, Diego and Sorkine, Olga and Shamir, Ariel},
title = {A Comparative Study of Image Retargeting},
year = {2010},
issue_date = {December 2010},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
volume = {29},
number = {6},
issn = {0730-0301},
url = {https://doi.org/10.1145/1882261.1866186},
doi = {10.1145/1882261.1866186},
abstract = {The numerous works on media retargeting call for a methodological approach for evaluating retargeting results. We present the first comprehensive perceptual study and analysis of image retargeting. First, we create a benchmark of images and conduct a large scale user study to compare a representative number of state-of-the-art retargeting methods. Second, we present analysis of the users' responses, where we find that humans in general agree on the evaluation of the results and show that some retargeting methods are consistently more favorable than others. Third, we examine whether computational image distance metrics can predict human retargeting perception. We show that current measures used in this context are not necessarily consistent with human rankings, and demonstrate that better results can be achieved using image features that were not previously considered for this task. We also reveal specific qualities in retargeted media that are more important for viewers. The importance of our work lies in promoting better measures to assess and guide retargeting algorithms in the future. The full benchmark we collected, including all images, retargeted results, and the collected user data, are available to the research community for further investigation at http://people.csail.mit.edu/mrub/retargetme.},
journal = {ACM Trans. Graph.},
month = {dec},
articleno = {160},
numpages = {10},
keywords = {benchmark, media retargeting, user study}
}

@article{streaming-video,
author = {Kr\"{a}henb\"{u}hl, Philipp and Lang, Manuel and Hornung, Alexander and Gross, Markus},
title = {A System for Retargeting of Streaming Video},
year = {2009},
issue_date = {December 2009},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
volume = {28},
number = {5},
issn = {0730-0301},
url = {https://doi.org/10.1145/1618452.1618472},
doi = {10.1145/1618452.1618472},
abstract = {We present a novel, integrated system for content-aware video retargeting. A simple and interactive framework combines key frame based constraint editing with numerous automatic algorithms for video analysis. This combination gives content producers high level control of the retargeting process. The central component of our framework is a non-uniform, pixel-accurate warp to the target resolution which considers automatic as well as interactively defined features. Automatic features comprise video saliency, edge preservation at the pixel resolution, and scene cut detection to enforce bilateral temporal coherence. Additional high level constraints can be added by the producer to guarantee a consistent scene composition across arbitrary output formats. For high quality video display we adopted a 2D version of EWA splatting eliminating aliasing artifacts known from previous work. Our method seamlessly integrates into postproduction and computes the reformatting in real-time. This allows us to retarget annotated video streams at a high quality to arbitary aspect ratios while retaining the intended cinematographic scene composition. For evaluation we conducted a user study which revealed a strong viewer preference for our method.},
journal = {ACM Trans. Graph.},
month = {dec},
pages = {1–10},
numpages = {10},
keywords = {art-directability, video retargeting, warping, content-awareness, user study, EWA splatting}
}

@inproceedings{seam-carving-initial,
    author = {Avidan, Shai and Shamir, Ariel},
    title = {Seam Carving for Content-Aware Image Resizing},
    year = {2007},
    isbn = {9781450378369},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    url = {https://doi.org/10.1145/1275808.1276390},
    doi = {10.1145/1275808.1276390},
    abstract = {Effective resizing of images should not only use geometric constraints, but consider the image content as well. We present a simple image operator called seam carving that supports content-aware image resizing for both reduction and expansion. A seam is an optimal 8-connected path of pixels on a single image from top to bottom, or left to right, where optimality is defined by an image energy function. By repeatedly carving out or inserting seams in one direction we can change the aspect ratio of an image. By applying these operators in both directions we can retarget the image to a new size. The selection and order of seams protect the content of the image, as defined by the energy function. Seam carving can also be used for image content enhancement and object removal. We support various visual saliency measures for defining the energy of an image, and can also include user input to guide the process. By storing the order of seams in an image we create multi-size images, that are able to continuously change in real time to fit a given size.},
    booktitle = {ACM SIGGRAPH 2007 Papers},
    pages = {10–es},
    keywords = {image retargeting, image resizing, display devices, content-aware image manipulation, image seams},
    location = {San Diego, California},
    series = {SIGGRAPH '07}
}

@inproceedings{warp,
author = {Wolf, Lior and Guttmann, Moshe and Cohen-Or, Daniel},
year = {2007},
month = {01},
pages = {1-6},
title = {Non-homogeneous Content-driven Video-retargeting},
volume = {1},
journal = {Computer Vision, 2007. ICCV 2007. IEEE 11th International Conference on},
doi = {10.1109/ICCV.2007.4409010}
}

@article{sns,
author = {Wang, Yu-Shuen and Tai, Chiew-Lan and Sorkine, Olga and Lee, Tong-Yee},
year = {2008},
month = {12},
pages = {118},
title = {Optimized Scale-and-Stretch for Image Resizing},
volume = {27},
isbn = {978-1-4503-1831-0},
journal = {ACM Transactions on Graphics (TOG)},
doi = {10.1145/1409060.1409071}
}

@article{multiop,
author = {Rubinstein, Michael and Shamir, Ariel and Avidan, Shai},
year = {2009},
month = {07},
pages = {},
title = {Multi-operator Media Retargeting},
volume = {28},
journal = {ACM Trans. Graph.},
doi = {10.1145/1576246.1531329}
}


@inproceedings{sm,
author = {Pritch, Yael and Kav-Venaki, Eitam and Peleg, Shmuel},
year = {2009},
month = {11},
pages = {151 - 158},
title = {Shift-Map Image Editing},
journal = {Proceedings of the IEEE International Conference on Computer Vision},
doi = {10.1109/ICCV.2009.5459159}
}


@article{lg,
author = {Karni, Z. and Freedman, Daniel and Gotsman, Craig},
year = {2009},
month = {08},
pages = {1257 - 1268},
title = {Energy‐Based Image Deformation},
volume = {28},
journal = {Computer Graphics Forum},
doi = {10.1111/j.1467-8659.2009.01503.x}
}