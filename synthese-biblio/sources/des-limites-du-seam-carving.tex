\section{Des limites du \emph{Seam Carving}}
\label{sec:limit}

Preuve de l'engouement suscité par \emph{Seam Carving}, des applications d’infographistes réputées telles que \emph{Adobe Photoshop CS4}, \emph{GIMP} où \emph{ImageMagick} l’ont intégré. Cependant il ne fait pas l’unanimité auprès des utilisateurs. L'article \emph{A Comparative Study of Image Retargeting} \cite{seam-carving-limites} écrit par cinq chercheurs présente une étude qui a mené à comparer huit méthodes de redimensionnements d’images, dont le \emph{Seam Carving}. 


\subsection{Déroulement de l'étude}

Afin de comparer ces algorithmes, les chercheurs ont réalisé deux sondages afin de récolter l’avis des participants vis-à-vis de certaines images redimensionnées. Ensuite, les chercheurs ont présenté une analyse sur les données récoltées. Enfin, ils ont tenté de généraliser ces résultats. Pour ce faire, ils ont cherché à vérifier si les résultats des mesures de distance entre images peuvent correspondre aux données des études réalisées.  L’objectif était de pouvoir prédire quel algorithme de redimensionnement d’image pourrait donner des résultats satisfaisants. 

\subsubsection{Choix des images}

Afin de sonder les utilisateurs, les scientifiques ont conçu une base de données d’images à redimensionner. La sélection des images est fondée sur les objectifs avancés par les algorithmes de redimensionnements déjà existants, qui sont :

\begin{enumerate}
\item la préservation du contenu important,
\item la préservation de la structure interne de l'image,
\item la prévention des artefacts visuels.
\end{enumerate} 

La structure interne d'une l'image correspond par exemple à ses éventuelles lignes géométriques, ses tons clairs ou sombres, ses différents plans ou bien sa profondeur.
Les images sélectionnées peuvent contenir des personnes, des visages, des lignes et/ou des bords clairs, des motifs répétitifs, des structures géométriques spécifiques, des éléments symétriques. Les chercheurs ont récolté 80 images dans leur base de données, classifiées en fonction de leur contenu.

\subsubsection{Les sondages}

Les chercheurs ont donc réalisé deux sondages différents. Lors du premier sondage, ils ont choisi la technique de \textbf{comparaison par paires}, dans laquelle les participants voient deux images redimensionnées côte à côte, et sont invités à choisir celle qu'ils préfèrent. Une interface \emph{web} leur a permis de basculer facilement entre les deux résultats redimensionnés afin de rendre les différences plus apparentes. Ils ont également affiché l'image originale. Ainsi, chaque paire d'images est comparée par le même nombre de participants. Au total, 210 participants ont pris part au test.

Un deuxième sondage a également été réalisé afin de déterminer si la connaissance de l’image originale affecte le résultat du redimensionnement préféré. Ainsi, les chercheurs ont mené une version à l'aveugle du même premier test, faisant intervenir 210 nouveaux participants, où l'image originale n'était pas affichée.
D’après ces scientifiques, cette dernière expérience transpose peut-être mieux la réalité, où les humains sont généralement exposés à des médias redimensionnés sans connaître l’image originale.

Afin de mieux comprendre les choix qui ont poussé au rejet d'une image redimensionnée, les participants ont parfois été invités à choisir une ou plusieurs propositions parmi un ensemble donné.

\subsection{Les résultats sur les données subjectives des sondés}

Les chercheurs ont constaté qu’il existe un consensus général parmi les sondés. 
Lorsque l’image originale n'est pas présentée, \emph{Seam Carving} est classé parmi les pires algorithmes de redimensionnement. Sans l’image originale, les participants ne savent pas qu’il manque de l'information aux images redimensionnées, qui de plus ne présentent pas d’artefacts visuels. Cette expérience apporte donc un net avantage aux méthodes de redimensionnement d’image utilisant du recadrage.

Ce résultat est équivalent si les organisateurs de l'étude présentent aux sondés l’image originale~: la perte de contenu est généralement préférée aux artefacts visuels de déformation. En effet, les algorithmes de redimensionnement en tête des préférences utilisent du recadrage, entraînant de la perte de contenu dans les images ciblées. De plus, d’après les sondés, les raisons du rejet des images redimensionnées par \emph{Seam Carving} sont la distorsion des lignes et des bords, et la déformation des personnes et des objets.

Les résultats de ces données sont retranscrites ci-dessous (figure \ref{fig:comparaisons_resultats_etude}), établissant le comparatif des algorithmes nommés \emph{Nonhomogeneous warping} (WARP) [\emph{Wolf et al. 2007} \cite{warp}], \emph{Seam-Carving} (SC) [\emph{Rubinstein et al. 2008} \cite{seam-carving-video}], \emph{Scale-andStretch} (SNS) [\emph{Wang et al. 2008} \cite{sns}], \emph{Multi-operator} (MULTIOP) [\emph{Rubinstein et al. 2009} \cite{multiop}], \emph{Shift-maps} (SM) [\emph{Pritch et al. 2009} \cite{sm}], \emph{Streaming Video} (SV) [\emph{Krähenbül et al. 2009} \cite{streaming-video}], et enfin \emph{Energy-based deformation} (LG) [\emph{Karni et al. 2009} \cite{lg}]. Nous nommons CR l'opération de recadrage.

\begin{figure}[H]
    \centering
    \includegraphics[height=0.4\linewidth]{resources/comparaisons_algos_de_redimentionnements_avec_reference.png}
    \includegraphics[height=0.4\linewidth]{resources/comparaisons_algos_de_redimentionnements_sans_reference.png}
    \caption{Résultats des études en fonction du contenu de l'image redimensionnée et de l'algorithme utilisé. À gauche, les résultats lorsque l'image de référence est présentée, à droite lorsqu'elle est cachée. Les algorithmes de redimensionnement d'image sont classés en fonction des votes reçus de gauche (plus de votes) à droite (moins de votes). Les algorithmes au sein d'un groupe sont statistiquement indiscernables en matière de préférence de l'utilisateur \cite{seam-carving-limites}.}
    \label{fig:comparaisons_resultats_etude}
\end{figure}

\subsection{Vers une analyse objective ?}

Les scientifiques ont tenté de comprendre si la différence entre deux images pouvait prédire nos préférences, afin de pouvoir anticiper quelle méthode de redimensionnement d’image pourrait donner objectivement des solutions satisfaisantes.
Ainsi, ils ont voulu estimer dans quelle mesure les résultats de dissimilitude entre deux images concordent avec les préférences subjectives des utilisateurs.

Ils ont observé un écart relativement important entre ces mesures et les données subjectives des sondés qui ont préféré l'algorithme \emph{Streaming Video}. Cet algorithme a reçu un faible classement par presque toutes les mesures de différence d’images. Leurs résultats montrent donc qu’ils sont encore loin d'imiter la \textbf{perception humaine}.

\subsection{Les nuances sur les résultats de l'étude}

Malgré ces données pessimistes sur les performances de \emph{Seam Carving}, il est à souligner que ces scientifiques n’ont pas considéré certaines optimisations existantes sur cet algorithme. Utiliser une méthode de détection des visages afin de sélectionner les zones où \emph{Seam Carving} ne doit pas agir pourrait permettre de limiter les artefacts visuels de déformation. Laisser à l'utilisateur le choix de sélectionner manuellement les zones où ce dernier ne doit pas s’appliquer permettrait également d’obtenir une optimisation équivalente.
De plus, les chercheurs ont présenté des images denses en contenu. Le redimensionnement choisi était conséquent, allant de 20 à 50\% de l’image originale. Dans ce contexte, \emph{Seam Carving} est propice aux artefacts visuels. Ainsi, les organisateurs de l'étude admettent que sur des redimensionnements plus petits, \emph{Seam Carving} obtient de bons résultats.
Autre fait, le sondage ne présentant pas l’image originale n’est peut-être pas représentatif de toutes les situations du monde réel. En effet, la perte d’information peut s’avérer problématique: l’image redimensionnée a peut-être perdu le message qu’a voulu transmettre son auteur.

