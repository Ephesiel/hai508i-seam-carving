\section{Énergie prospective}
\label{sec:forward-energy}

Nous avons vu dans la section précédente comment fonctionne le \emph{Seam Carving} et quelles sont ses applications.

Quelques années après la publication de leur premier article, les auteurs du \emph{Seam Carving} améliorent ce dernier en l'étendant aux vidéos \cite{seam-carving-video}. Pour ce faire, ils définissent le concept de \emph{forward energy} que nous traduiront en français par \textbf{énergie prospective}.\\

C'est ce concept d'énergie prospective que nous allons décrire dans cette section, comment il améliore l'algorithme de base du \emph{Seam Carving}, quels sont ses qualités et ses défauts \cite{blog-forward-energy}.

\subsection{Motivation}

Bien qu'il produise un résultat sans coupure nette, l'algorithme vu précédemment, introduit encore quelques artefacts visuels sur certaines images. En effet, comme nous pouvons l'observer sur la figure ~\ref{fig:arch}, la forme de l'avant-dernier rocher à droite de l'image a perdu toute sa forme. Sa base a été affinée, et une ligne laissant traverser les lueurs du ciel semble trancher une partie de sa façade.

Le principal problème de \textbf{l'énergie rétrospective} (\emph{backward energy}, le concept vu dans la section précédente) est qu'elle suggère quelle couture il faut supprimer, mais qu'elle ne tient pas compte du rendu final. En supprimant une couture à faible énergie, des pixels qui n'étaient pas adjacents sont rapprochés, et l'énergie totale de l'image peut augmenter considérablement.

% TO DO : Ajouter une figure pour montrer une différence concrète de pourquoi l'énergie peut augmenter dans l'image

Au contraire, l'énergie prospective prédit quels pixels seront adjacents après la suppression d'une couture et s'en sert pour suggérer la meilleure couture à supprimer.

\subsection{Définition pour un pixel}

Imaginons que l'énergie d'un pixel se calcule en fonction de la différence de couleur entre ce pixel et les pixels adjacents. Un pixel se décompose en trois octets : $p = (R_p, V_p, B_p)$. On peut définir la différence $\mathcal{D}$ entre deux pixels $p_0$ et $p_1$ comme ceci :
$$\mathcal{D}(p_0, p_1) = (R_{p_0} - R_{p_1})^2 + (V_{p_0} - V_{p_1})^2 + (B_{p_0} - B_{p_1})^2.$$

Nous cherchons à définir le changement d'énergie que procure la suppression d'un pixel de l'image. Pour ce faire, il faut savoir quel pixel a été choisi sur la ligne supérieure (nous nous posons dans le cas d'une suppression de couture verticale).\\

\begin{itemize}
    \item Soit c'est le pixel juste au dessus.
    \item Soit c'est le pixel en haut à gauche.
    \item Soit c'est le pixel en haut à droite.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{resources/seam-removal-new-edges.png}
    \caption{Nouvelles jonctions (en rouge) après la suppression des pixels (en bleu)}
    \label{fig:seam-removal}
\end{figure}

On peut voir sur la figure~\ref{fig:seam-removal} qu'après suppression de la couture et en fonction des pixels choisis, de nouvelles jonctions apparaissent. Ce sont ces nouvelles jonctions qui définissent l'énergie prospective du pixel supprimé.

\textbf{Note :} On remarque que les nouvelles jonctions de la ligne $y - 1$ ne sont pas comptabilisées. En effet, elles sont déjà calculées dans l'énergie prospective du pixel supprimé de la ligne précédente.

Le coût de la suppression d'un pixel est la somme des nouvelles jonctions. Ainsi, au lieu d'une seule énergie par pixel, nous avons trois coûts distincts en fonction du pixel choisi sur la ligne supérieure :

\begin{align*}
    \mathcal{C_L}(x, y) &= \mathcal{D}((x - 1, y), (x + 1, y)) + \mathcal{D}((x - 1, y), (x, y - 1)),\\
    \mathcal{C_U}(x, y) &= \mathcal{D}((x - 1, y), (x + 1, y)),\\
    \mathcal{C_R}(x, y) &= \mathcal{D}((x - 1, y), (x + 1, y)) + \mathcal{D}((x + 1, y), (x, y - 1)).
\end{align*}

% TO DO : Ajouter les formules pour les colonne de droite et gauche qui n'ont pas exactement les mêmes formules (voir le papier)

Le coût $\mathcal{C_L}$ correspond au pixel de gauche, $\mathcal{C_U}$ celui du dessus et $\mathcal{C_R}$ celui de droite.

\subsection{Calcul de la meilleure couture}

De manière analogue à l'algorithme du \emph{Seam Carving}, nous pouvons ici utiliser la programmation dynamique pour calculer quelle couture est la meilleure.\\

On note $\mathcal{F_E}$ l'énergie prospective d'un pixel. Initialement, pour la première ligne, aucun pixel n'a été choisi précédemment. La seule jonction qui serait ajoutée à l'image est celle entre les pixels de gauche et de droite. On a donc :

\begin{align*}
&\mathcal{F_E}(x, 0) = \mathcal{D}((x - 1, 0), (x + 1, 0)) = \mathcal{C_U}.
\end{align*}

Ensuite, pour tous les autres pixels de la matrice, on préférera prendre l'énergie minimale entre les trois énergies calculées \footnote{Attention toutefois aux pixels de la première et dernière colonne qui ne peuvent choisir qu'entre deux énergies.}.

\begin{equation*}
 \mathcal{F_E}(x, y) = \min
    \begin{cases}
        \mathcal{F_E}(x - 1, y - 1)  &+\ \mathcal{C_L}(x, y)\\
        \mathcal{F_E}(x, y - 1)  &+\ \mathcal{C_U}(x, y)\ \ \ .\\
        \mathcal{F_E}(x + 1, y - 1)  &+\ \mathcal{C_R}(x, y)
    \end{cases}
\end{equation*}

Une fois que nous connaissons l'énergie prospective des $n\times m$ pixels de l'image, il suffit de récupérer le pixel $(x, m - 1)$ ayant la plus basse énergie et \emph{backtrack} pour connaître la couture à supprimer.

Nous pouvons voir sur les figures~\ref{fig:arch} et~\ref{fig:arch-forward} la comparaison des résultats en utilisant respectivement l'énergie rétrospective et prospective. L'avant-dernier rocher à droite sur la figure~\ref{fig:arch-forward} ne présente plus les défauts précédemment énoncés.

\begin{figure}[p]
    \centering
    \includegraphics[height=0.45\linewidth]{resources/arch.jpg}
    \includegraphics[height=0.45\linewidth]{resources/arch-resized.jpg}
    \caption{Image redimensionnée avec l'énergie retrospective - Formation rocheuse dans le parc national des Arches - présence de problèmes de distorsions}
    \label{fig:arch}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[height=0.45\linewidth]{resources/arch.jpg}
    \includegraphics[height=0.45\linewidth]{resources/arch-resized-forward-energy.jpg}
    \caption{Image redimensionnée avec l'énergie prospective - Formation rocheuse dans le parc national des Arches}
    \label{fig:arch-forward}
\end{figure}

\subsection{Critique}

L'énergie prospective comme définie par ses auteurs ne prend pas en compte les pixels supprimés. Si nous devions recalculer complètement le changement d'énergie de l'image, l'énergie des pixels supprimés devrait être utilisée dans le calcul, ce qui donnerait encore un résultat différent.

Cet algorithme n'améliore pas le \emph{Seam Carving} mais le complète. En effet, l'imagerie n'étant pas une science exacte, certaines photos peuvent mieux rendre avec l'algorithme de base.

Enfin, il faut savoir que l'énergie prospective ne peut pas fonctionner aisément avec tout type de fonction d'énergie. Dans notre exemple nous avons utilisé une fonction simple qui calculait l'énergie en fonction des pixels voisins, mais certaines fonctions n'utilisent pas la notion de pixel voisin, ce qui rend l'énergie prospective inefficace (ou extrêmement longue, entraînant un recalcul de l'énergie pour chaque suppression de pixel).