\section{Principe du \emph{Seam Carving}}
\label{sec:seam-carving}

Dans cette partie, nous allons expliquer le fonctionnement du \emph{Seam Carving} avant d'aborder les différentes utilisations qui peuvent en être faites.

\subsection{Définition et fonctionnement}
 Puisqu'il s'agit d'un outil \emph{content-aware}, il a pour objectif de minimiser la perte d'information. Nous nous intéressons à la façon dont l'algorithme trie les pixels afin de construire l'ensemble de ceux qui seront supprimés. Nous commencerons donc par définir cet ensemble, appelé \emph{seam} ou \textbf{couture} en français, puis nous étudierons la façon de le construire.
 
\subsubsection{Définition d'une couture}
Dans leur article, Ariel Shamir et Shai Avidan définissent une couture verticale par intention avec la formule 

$$s^x = \{s_i^x\}_{i=1}^n = \{x(i),\ i\}_{i=1}^n,$$

\noindent
où $i$ est l'indice en ordonnée du pixel, ainsi qu'une contrainte sur l'ensemble obtenu (figure \ref{fig:exemple_couture_valide}).

Une couture verticale $s^x$ est donc un ensemble de $n$ pixels où $n$ est la hauteur de l'image à redimensionner.
Pour qu'un tel ensemble soit une couture, il faut que ses éléments respectent une contrainte de proximité. Ainsi, pour deux rangs horizontaux $i$ et $i + 1$ consécutifs de pixels, leurs pixels correspondants dans une couture, $x(i)$ et $x(i + 1)$ doivent être connectés :

$$\forall(i),\ |x(i) - x(i+1)|\ \leq \ 1.$$

 La définition de la couture horizontale est analogue. 

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{resources/PrincipeF1.pdf}
    \caption{Contrainte sur un ensemble de pixels : couture valide à gauche et ensemble quelconque de pixels à droite}
    \label{fig:exemple_couture_valide}
\end{figure}

Les coutures sont donc des chemins de pixels qui traversent l'image de haut en bas, ou de gauche à droite pour le cas horizontal. Au redimensionnement, ce sont les pixels de la couture qui sont supprimés ou dupliqués et les deux morceaux d'image restants sont recousus ensemble. En enlevant une couture verticale d'une image de longueur $m$, l'algorithme en renvoie une nouvelle de longueur $m-1$.

\subsubsection{Choix de la couture optimale}
Puisque nous connaissons maintenant la forme de l'ensemble des pixels à supprimer lors de l'utilisation du \emph{Seam Carving}, nous pouvons nous intéresser à la façon dont ils sont sélectionnés. L'algorithme fait ce choix en trois étapes.

Pour minimiser l'information perdue, il faut tout d'abord savoir situer les points d'intérêt dans l'image. Il faut pour cela avoir recours à des fonctions qui calculent pour chaque pixel son énergie. Ces fonctions d'énergie sont nombreuses et influencent le résultat du redimensionnement, mais pas le principe du \emph{Seam Carving}. Nous ne nous étendrons donc pas sur le sujet ici, mais la partie  \ref{sec:energy} : \nameref{sec:energy} explique le fonctionnement d'une de ces fonctions.

Une fois que cette donnée est connue, l'algorithme se sert du concept de \textbf{programmation dynamique} pour sélectionner la couture la moins intéressante. Il s'agit de celle dont l'énergie est la plus faible, l'énergie d'une couture étant la somme de celle de ses pixels. Pour cela, la deuxième étape va donc être de  parcourir la matrice des pixels de haut en bas (de gauche à droite pour une couture horizontale). Pour chaque pixel situé colonne $i$ du rang $j$ , son énergie va être augmentée de celle du pixel candidat le plus faible du rang précédent. Cette nouvelle valeur est appelée \textbf{énergie rétrospective} du pixel. L'algorithme utilise une fonction de minimisation M présentée en figure \ref{fig:energie_retrospective}. 

\begin{figure}[H]
\centering
   \begin{minipage}{.5\textwidth}
    $$M(i, j)\ =\ e(i, j) + \max
        \begin{cases}
            {M(i-1, j-1)}\\
            {M(i, j-1)}\ \ \ \ \ \ .\\
            {M(i+1, j-1)}
        \end{cases}$$
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
        \centering
        \includegraphics[width=\linewidth]{resources/PrincipeF2.pdf}
    \end{minipage}
    \caption{Calcul de l'énergie rétrospective d'un pixel avec la fonction de minimisation M à partir de son énergie $e(i, j)$}
    \label{fig:energie_retrospective}
\end{figure}

L'objectif de cette deuxième étape est de situer le pixel du dernier rang dont l'énergie rétrospective est la plus faible. La troisième et dernière étape consiste à utiliser ce pixel comme point d'arrivée de la couture, avant de remonter rang par rang. Les pixels d'énergie minimale vont à nouveaux être sélectionnés parmi les candidats potentiels, pour former la couture finale à supprimer.

\subsection{Utilisations}
Jusqu'à présent, pour simplifier l'explication du fonctionnement du \emph{Seam Carving}, nous nous ne sommes intéressés qu'au retrait d'une couture verticale pour réduire d'un pixel la longueur d'une image. Toutefois cet outil ne se limite pas à la réduction~: il permet aussi l'amplification ainsi que la suppression ciblée.

\subsubsection{Réduction de taille et ciblage}
La fonctionnalité principale des outils de redimensionnement est la réduction de taille, et le \emph{Seam Carving} n'y fait pas défaut. S'il s'agit d'une réduction sur une seule dimension, il suffit de répéter $d$ fois l'opération de retrait de couture, où $d$ est la différence entre la taille initiale et la taille souhaitée.
Pour une réduction en deux dimensions, qu'on peut appeler ciblage, il va également falloir enlever $d$ coutures, où  $d = d_1 + d_2$ avec $d_1$ la réduction souhaitée en longueur et $d_2$ la réduction souhaitée en largeur. La question qui se pose est alors celle de l'ordre de suppression entre les coutures verticales et horizontales. Pour rester fidèle à l'objectif initial de conservation d'information, c'est la couture la moins intéressante qui va être enlevée en premier, indépendamment de son orientation.

\subsubsection{Augmentation de taille et amplification}
Le \emph{Seam Carving} permet également d'augmenter la taille d'une image. Cette opération se fait par insertions successives de coutures. Pour comprendre comment ces pixels insérés sont générés par l'algorithme, il faut se représenter le redimensionnement d'image comme une frise chronologique. L'image est dans son état initial au temps $T$, à chaque suppression de couture ce temps augmente d'une unité. Pour insérer $d$ coutures, il suffit alors de considérer l'image initiale comme étant au temps $T + d$, et de simuler un retour dans le temps (figure \ref{fig:evolution_image}).

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{resources/PrincipeF3.pdf}
    \caption{Évolution d'une image dans le temps}
    \label{fig:evolution_image}
\end{figure}

Ainsi, l'algorithme va chercher la couture minimale et la dupliquer pour augmenter la taille d'un pixel. Il obtiendra une nouvelle image et pourra répéter l'opération d'insertion de pixels jusqu'à obtenir la taille requise. Chaque nouvelle duplication devra cependant se faire à partir d'une nouvelle couture minimale différente de la précédente, pour ne pas insérer les mêmes pixels à chaque itération.
En combinant l'augmentation de taille et le ciblage, vu plus tôt, nous obtenons une nouvelle opération : l'amplification. En effet, effectuer successivement un ciblage (qui rétrécit l'image) et une augmentation pour la ramener à sa taille initiale, revient à zoomer sur la zone d'intérêt de l'image.

\subsubsection{Autres utilisations}
Enfin, l'algorithme du \emph{Seam Carving} permet de mettre en place des fonctionnalités qui sortent du cadre des outils de redimensionnement classiques.
D'une part, il est possible de faire de la suppression d'objets, ou plus précisément de la suppression de pixels ciblés. Pour cela, il faut proposer une interface à l'utilisateur pour qu'il sélectionne sur l'image les pixels qu'il souhaite voir disparaître. L'algorithme calcule alors l'orientation des coutures à supprimer de façon à perdre le moins d'information possible (hors de la zone sélectionnée), comme lors d'un ciblage, puis effectue les suppressions. Pour retrouver la taille initiale de l'image, l'agrandissement peut être utilisé.
D'autre part, le \emph{Seam Carving} pourrait permettre de proposer un redimensionnement sur des vidéos plutôt que sur de simples images. Cette fonctionnalité est le sujet de la partie~\ref{sec:real-time} : \nameref{sec:real-time}.