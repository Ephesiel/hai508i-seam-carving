\section{Redimensionnement en temps réel}
\label{sec:real-time}

\subsection{Limite temporelle du \emph{Seam Carving}}
La méthode de \emph{Seam Carving} présentée par Avidan et Shamir s'avère trop complexe pour envisager de l'utiliser en temps réel, et ce à cause des multiples étapes préliminaires de programmation dynamique durant le calcul des chemins. Pour une image de taille $n \times m$, l'algorithme de calcul de la couture optimale est de complexité $O(n^2m)$ ou $O(m^2n)$ selon si on cherche des coutures horizontales ou verticales. Cet algorithme est relancé $c$ fois (le nombre de coutures recherchées), d'où une complexité totale en $O(cn^2m)$ ou $O(cnm^2)$.

Avidan et Shamir proposent d'opérer cette étape en un seul calcul pour accélérer significativement l'algorithme et ouvrir la voie au \emph{Seam Carving} de vidéos en temps réel.

Pour parvenir à redimensionner en temps réel, il faudrait pouvoir stocker des coutures horizontales et verticales. Cependant, si elles se croisent en plus d'un point, le retrait des unes pourrait entraîner la "destruction" des autres, ce qui obligerait à recommencer le calcul. 

Ils proposent pour éviter cela d'établir des \textbf{relations de correspondance} entre chaque ligne de pixels, et d'ajouter des contraintes. Pour calculer les relations de correspondance, ils utilisent l'algorithme hongrois - permettant de calculer un couplage de poids maximum dans un graphe biparti pondéré, dont les auteurs de \emph{Real-time content-aware image resizing}\cite{Huang2009RealtimeCI} constatent la lenteur. Ceux-ci proposent une autre méthode, utilisant une fonction de correspondance entre les pixels. 

Nous allons expliciter cette fonction, comment elle prend appui sur une fonction de pondération, et de quelle manière elle permet un calcul des coutures plus rapide. 

\subsection{Nouvel algorithme}

Huan et al. proposent l'algorithme suivant:
 \begin{enumerate}
\item établir la matrice d'énergie de chaque pixel,
\item créer des relations de correspondance entre pixels, ligne à ligne, pour trouver toutes les coutures en parallèle,
\item calculer l'énergie cumulée de chaque couture et les retirer ou dupliquer dans l'ordre croissant.
\end{enumerate} 

Nous remarquons que la différence notable se situe sur la seconde étape, l'établissement de relations de correspondance.

Afin d'établir des relations de correspondance, il faut coupler chaque pixel à un unique pixel de la ligne au dessus. Pour $I(i,j)$ le pixel en position $(i,j)$, avec $i$ la colonne et $j$ la ligne de l'image $I$, $m$ la fonction de correspondance telle que $m(i,j) = k$ avec $k$ la colonne du pixel couplé, nous avons $I(m(i, j), j+1)$ définissant la position du pixel couplé au pixel $I(i,j)$. À cela s'ajoute la contrainte
\begin{align*}
    \forall  i \in [1, m ] \land j \in [1, n ] ,   |m(i, j) - i| \leq 1 \ .
\end{align*}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.73\linewidth]{resources/appariement-lignes.png}
    \caption{Calcul des paires de pixels ligne à ligne \cite{Huang2009RealtimeCI}}
    \label{fig:graph-biparti}
\end{figure}
Pour ce faire, les auteurs établissent un calcul de poids pour chaque arête du graphe biparti pondéré composé des noeuds-pixels des deux lignes pour lesquelles ils cherchent des correspondances (figure \ref{fig:graph-biparti}, le pixel $I(m,k)$ est couplé aux pixel $I(m-1,k+1)$). Ensuite, ils utilisent une méthode de répartition prenant en compte la contrainte vue ci-dessus, et de ce fait plus simple que celle opérée par l'algorithme hongrois.

\subsubsection{Pondération des arêtes}

La première version de la formule de calcul de poids cherche à maximiser la variance des énergies aux extrémités des arêtes pour une ligne donnée. Toutefois les coutures qu'elle génère sont trop verticales (figure \ref{fig:weight-methods} à gauche). La seconde, finalement retenue comme plus satisfaisante (figure \ref{fig:weight-methods} à droite), intègre la matrice $M$ produite par programmation dynamique de l'algorithme original. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.92\linewidth]{resources/weight-methods.png}
    \caption{Coutures générées par les deux fonctions de poids différentes  \cite{Huang2009RealtimeCI}}
    \label{fig:weight-methods}
\end{figure}

La formule du calcul de poids des arêtes à la ligne $k$ est donc $w(i,j)$ avec $i$ et $j$ les colonnes des pixels considérés. $M(i,j)$ représente l'énergie cumulée de tous les pixels appartenant à la couture optimale partant du pixel $I(i,0)$, et allant jusqu'à la dernière ligne. La matrice $A$ est calculée au fur et à mesure de l'appariement des lignes. $A(i,j)$ représente l'énergie cumulée de tous les pixels de la couture partant de la première ligne et allant jusqu'au pixel $I(i,j)$. La \textbf{fonction de poids} se définit ainsi:
\begin{equation*}
    w(i,j ) =
    \begin{cases}
        \mathcal{A}(i, k) \times \mathcal{M}(j, k+1) &\  si \  |i-j| \leq 1 \ ,\\
        -\infty &\ sinon .
    \end{cases}
\end{equation*}

\subsubsection{Calcul des correspondances}
Pour trouver les correspondances, on note $\mathcal{F}(x)$ la somme optimale des poids des arêtes des $x$ premières paires de pixels, telle que:
\begin{align*}
    \mathcal{F}(i) =\  \max \{\ 
        \mathcal{F}(i - 1) + w(i, i) , \  
        \mathcal{F}(i-2) + w(i, i-1) + w(i-1, i) \  \} \ .
\end{align*}

Pour une image de 1024x768px, l'implémentation donne un résultat en 100ms (contre 12s par l'algorithme original). Par l'exemple, les auteurs montrent que le résultat est quasiment le même que celui obtenu par l'algorithme original pour des réductions de moins de 50\% de la hauteur ou largeur. Pour des réductions plus importantes, la qualité se dégrade. La figure \ref{fig:seam-carving-original} présente une application de l'algorithme original et de l'algorithme en temps réel. Nous pouvons remarquer sur la figure la plus à droite, redimensionné avec l'algorithme en temps réel, des déformations au niveau des reflets des bâtiments dans l’eau. Une coupure dans les nuages est aussi apparente. Les deux bâtiments semblent avoir été coupé, puis recollé côte à côte, formant ainsi une ligne verticale dont les couleurs de part et d’autres de celle-ci ne se juxtaposent pas. Ces défauts semblent moins apparents sur les images à gauche, redimensionnés avec l'algorithme original.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\linewidth]{resources/seam-carving-original.png}
    \includegraphics[width=0.45\linewidth]{resources/seam-carving-real-time.png}
    \caption{Algorithme original (à gauche), algorithme en temps réel (à droite) \cite{Huang2009RealtimeCI}}
    \label{fig:seam-carving-original}
\end{figure}